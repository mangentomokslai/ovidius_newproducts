<?php
class Ovidius_NewProducts_Block_Newproducts extends Mage_Catalog_Block_Product_Abstract
{
	public function getNewProducts()
	{
        $productCollection = Mage::getModel('catalog/product')->getCollection()
					->addAttributeToSelect('name')
                    ->addAttributeToSelect('product_url')
					->addStoreFilter($this->getStoreId())
					->addAttributeToSort('created_at', 'desc')
					->setPageSize(5)
					->setCurPage(1);

		return $productCollection;
	}
}
